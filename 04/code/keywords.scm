;; a simple procedure for displaying values
(define (test value)
   (display value)
   (newline))

;; some fancy definitions
(define x 1)
(define $?=... +)
(define + 1)
(define define 2)

;; and the testing
(test x)
(test ($?=... x 1))
(test ($?=... + x x))
(test ($?=... define define))
