program realinteger

real integer  ! real variable named 'integer'
integer real  ! integer variable named 'real'

real = 0
integer = 0

write (*,*) "real: ", real
write (*,*) "integer: ", integer

end program realinteger
