program integer  ! program named 'integer'

integer program, subroutine, if, end  ! integer variables
type :: type  ! complex type named 'type'
  integer integer  ! integer member named 'integer'
end type
type (type) :: else, write  !  variables of the type named 'type'

program = 1
subroutine = 2
if = 3
end = 4
else%integer = 5  ! assign to the integer member of else
write%integer = 6
write (*,*) program, subroutine, if, end, else, write

end program integer
