local
   X = x(id:x)
   Y = y(id:y x:X)
   proc {Investigate R}
      if {Record.is R} then
	 {Browse record(label:{Record.label R}
			features:{Record.arity R})}
      end
   end
in
   {Investigate X}
   {Investigate Y}
end