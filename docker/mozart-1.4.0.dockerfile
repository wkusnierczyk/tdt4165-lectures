FROM 32bit/ubuntu:14.04
MAINTAINER "Waclaw Kusnierczyk" "waclaw.kusnierczyk@gmail.com"

ENV DEBIAN_FRONTEND='noninteractive'

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 3E5C1192
RUN apt-get update
RUN apt-get install -y alien=8.90
RUN apt-get install -y dpkg-dev=1.17.5ubuntu5.6
RUN apt-get install -y debhelper=9.20131227ubuntu1
RUN apt-get install -y build-essential=11.6ubuntu6
RUN apt-get install -y emacs24-nox=24.3+1-2ubuntu1
RUN apt-get install -y dc=1.06.95-8ubuntu1
RUN apt-get install -y git=1:1.9.1-1ubuntu0.3

WORKDIR /opt/mozart
RUN curl -O -L https://sourceforge.net/projects/mozart-oz/files/v1/1.4.0-2008-07-03-GENERIC-i386/mozart-1.4.0.20080704-16189.i386.rpm
RUN alien -i mozart-1.4.0.20080704-16189.i386.rpm

RUN apt-get install -y gfortran=4:4.8.2-1ubuntu6
RUN apt-get install -y mit-scheme=9.1.1-5
RUN apt-get install -y guile-2.0=2.0.9+1-1ubuntu1

WORKDIR /usr/lib/mozart/bin
RUN mv ozplatform ozplatform.orig
RUN (echo '#!/usr/bin/env bash'; echo 'linux32 /usr/lib/mozart/bin/ozplatform.orig') > ozplatform
RUN chmod 755 ozplatform

RUN useradd -ms /bin/bash mozart
RUN echo mozart:mozart | chpasswd
RUN adduser mozart sudo
USER mozart
ENV PATH="$PATH:/usr/lib/mozart/bin"
WORKDIR /home/mozart
CMD bash
