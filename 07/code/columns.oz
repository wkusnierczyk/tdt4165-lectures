fun {Columns Matrix}
   case Matrix of nil|_ then nil
   else
      {Map Matrix fun {$ Head|_} Head end}
      |{Columns {Map Matrix fun {$ _|Tail} Tail end}} end end