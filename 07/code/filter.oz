fun {Filter List Criterion}
   case List of Head|Tail
   then if {Criterion Head} then
	   Head|{Filter Tail Criterion}
	else {Filter Tail Criterion} end
   else nil end end
