def queue():
   def enclose(content):
      def self(message, *args):
         return self.__dict__[message](*args)
      self.enqueue = lambda item: enqueue(content, item)
      self.dequeue = lambda: dequeue(content)
      self.isempty = lambda: content == []
      self.preview = lambda: content[:]
      return self
   def enqueue(content, item):
      copy = content[:]
      copy.append(item)
      return enclose(copy)
   def dequeue(content):
      copy = content[1:]
      return enclose(copy), content[0]
   return enclose([])
