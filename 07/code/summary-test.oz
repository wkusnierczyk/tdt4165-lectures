declare

\insert select.oz
\insert foldleft.oz
\insert identity.oz
\insert iterate.oz
\insert summary.oz
\insert statistics.oz
\insert columns.oz

Matrix = [[1.0 2.4 3.5]
	  [3.2 2.3 4.1]]

{Browse {Summary Matrix [Min Max Sum Average]}}