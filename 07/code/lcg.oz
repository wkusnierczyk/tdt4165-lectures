fun {MakeLCG A C M}
   fun {$ Count Seed}
      fun {Iterate X N}
	 if N == 0 then nil
	 else
	    Xn = (A*X+C) mod M in
	    Xn|{Iterate Xn N-1} end end in
      {Iterate Seed Count} end end
LCG = {MakeLCG 1103515245 12345 {Number.pow 2 32}}