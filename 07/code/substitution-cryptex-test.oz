declare

\insert split.oz
\insert quicksort.oz
\insert zip.oz
\insert enumerate.oz
\insert substitution.oz
\insert substitution-cryptex.oz
\insert lcg.oz

Plaintexts = ["hellodolly" "funnybunny"]
Cryptexes =
{Map [1 2 3]
 fun {$ Seed} {MakeCryptex {LCG 26 Seed}} end}

Test =
{Map Plaintexts
 fun {$ Plaintext}
    {Map Cryptexes
     fun {$ Cryptex}
	Cryptogram = {Cryptex.encrypt Plaintext} in
	test(original:Plaintext
	     encrypted:Cryptogram
	     decrypted:{Cryptex.decrypt Cryptogram}) end} end}

{Browse Test}