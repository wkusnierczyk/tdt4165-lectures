fun {Max List}
   case List of [Value] then Value
   [] Value|Values then
      PartialMax = {Max Values} in
      if Value>PartialMax then Value else PartialMax end end