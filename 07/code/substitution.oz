fun {Encrypt Plaintext Key}
   {Map Plaintext
    fun {$ Character}
       {Nth Key (Character - &a + 1)}.2 end} end
fun {Decrypt Cryptogram ReverseKey}
   {Map Cryptogram
    fun {$ Character}
       {Nth ReverseKey (Character - &A + 1)}.2 end} end