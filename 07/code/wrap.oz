local
   SecretKey = {NewName} in
   fun {Wrap Data}
      fun {$ Key}
	 if Key == SecretKey then Data end end end
   fun {Unwrap SecureData}
      {SecureData SecretKey} end
end