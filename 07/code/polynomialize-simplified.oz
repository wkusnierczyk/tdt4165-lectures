fun {Polynomialize Coefficients Variables}
   {Flatten
    {Map Variables
     fun {$ Variable}
	{Map Coefficients
	 fun {$ coeff(A B C)}
	    A*Variable*Variable + B*Variable + C end} end}} end