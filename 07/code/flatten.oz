fun {Flatten Lists}
   case Lists of List|Lists
   then {Append List {Flatten Lists}}
   else nil end end