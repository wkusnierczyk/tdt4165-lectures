fun {VersatileCombine Objects Combine}
   case Objects of (_|_)|_ then
      {Mux Objects
       fun {$ Objects} {VersatileCombine Objects Combine} end}
   else {Combine Objects} end end