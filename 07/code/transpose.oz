fun {Transpose Matrix}
   case Matrix of nil|_ then nil
   else {Map Matrix Car}|{Transpose {Map Matrix Cdr}} end end