fun {FoldLeft List Null Transform Combine}
   {Iterate List#Null
    fun {$ List#_} List == nil end
    fun {$ (Head|Tail)#Result}
       Tail#{Combine Result {Tranform Head}} end}.2 end