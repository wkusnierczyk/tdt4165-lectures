fun {MatrixProduct Matrix1 Matrix2}
   {CrossMap Matrix1 {Transpose Matrix2}
    fun {$ Row Column}
       {Sum {VersatileCombine [Row Column] Product}} end} end