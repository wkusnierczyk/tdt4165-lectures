#!/usr/bin/env python

from random import random

statistics = [min, max, sum, lambda data: sum(data)/len(data)]

def generate(rowcount, colcount):
	return [[random() for col in range(colcount)] \
	        for row in range(rowcount)]
	        
def columns(matrix):
	return [map(lambda row: row[column], matrix) \
	        for column in range(len(matrix[0]))]	        
	       
def summary(matrix, statistics):
	return [[statistic(column) for statistic in statistics] \
	        for column in columns(matrix)]
	        
matrix = generate(3,3)
print matrix
print summary(matrix, statistics)
