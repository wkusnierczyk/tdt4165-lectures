fun {New}
   nil end
fun {Push Stack Item}
   Item|Stack end
fun {Pop Top|Rest ?Item}
   Item = Top
   Rest end
fun {IsEmpty Stack}
   Stack == nil end