declare

\insert polynomialize-simplified.oz

Coefficients = [coeff(1 2 3) coeff(3 2 1)]
Variables = [3 2 10]

{Browse {Polynomialize Coefficients Variables}}