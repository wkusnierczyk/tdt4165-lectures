fun {Columns Matrix}
   case Matrix of nil|_ then nil
   else {Map Matrix Car}|{Columns {Map Matrix Cdr}} end end