fun {MakeCryptex Key}
   cryptex(
      encrypt:fun {$ Plaintext} {Encrypt Plaintext Key} end
      decrypt:fun {$ Cryptogram} {Decrypt Cryptogram Key} end) end
