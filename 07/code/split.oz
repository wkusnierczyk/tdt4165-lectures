fun {Split List Pivot Before}
   case List
   of Head|Tail then
      Below#Above = {Split Tail Pivot Before} in
      if {Before Head Pivot} then (Head|Below)#Above
      else Below#(Head|Above) end
   else nil#nil end end