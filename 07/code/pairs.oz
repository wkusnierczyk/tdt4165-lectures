fun {Cons Head Tail} Head|Tail end
fun {Car Head|_} Head end
fun {Cdr _|Tail} Tail end