fun {Map List Function}
   Result#nil =
   {Iterate
    state(input:List
	  output:local End in End#End end)
    fun {$ state(input:Input ...)}
       Input == nil end
    fun {$ state(input:Head|Tail output:Start#End)}
       NewEnd in
       End = {Function Head}|NewEnd
       state(input:Tail
	     output:Start#NewEnd) end}.output in
   Result end