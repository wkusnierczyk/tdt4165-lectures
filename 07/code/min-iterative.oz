fun {Min List}
   {Iterate
    state(min:List.1
	  list:List.2)
    fun {$ state(list:List ...)} List == nil end
    fun {$ state(list:Head|Tail min:Min)}
       state(list:Tail
	     min:if Min<Head then Min else Head end) end}.min end