fun {Zip List1 List2}
   case List1#List2
   of (Head1|Tail1)#(Head2|Tail2) then
      (Head1#Head2)|{Zip Tail1 Tail2}
   else nil end end
fun {Unzip List}
   case List
   of (Head1#Head2)|Tail then
      Tail1#Tail2 = {Unzip Tail} in
      (Head1|Tail1)#(Head2|Tail2)
   else nil#nil end end