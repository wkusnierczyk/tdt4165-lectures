(define (mux combine lists)
   (if (null? (car lists))
       '()
       (cons (apply combine (map car lists))
             (mux combine (map cdr lists)))))

(mux + '((1 2 3) (4 5 6)))

(define (mux combine . lists)
   (if (null? (car lists))
       '()
       (cons (apply combine (map car lists))
             (apply mux (cons combine (map cdr lists))))))             
             
(mux + '(1 2 3) '(4 5 6))

(define (transpose matrix)
   (if (null? (car matrix))
       '()
       (cons (map car matrix)
             (transpose (map cdr matrix)))))

(define (mux combine . lists)
   (map (lambda (list) (apply combine list))
        (transpose lists)))
   
(mux + '(1 2 3) '(4 5 6))
