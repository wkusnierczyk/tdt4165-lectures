fun {Enumerate From To}
   if From > To then nil
   else From|{Enumerate From+1 To} end end