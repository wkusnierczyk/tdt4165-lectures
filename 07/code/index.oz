fun {Index List Item}
   fun {Iterate Items N}
      case Items
      of !Item|_ then N
      [] _|Items then {Iterate Items N+1}
      else raise noSuchElement(list:List item:Item) end end end in
   {Iterate List 1} end