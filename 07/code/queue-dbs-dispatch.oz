New =
local
   fun {Enqueue Start#End#Length Item}
      NewEnd in End = Item|!!NewEnd
      Start#NewEnd#(Length+1) end
   fun {Dequeue (Head|Tail)#End#Length ?Item}
      Item = Head Tail#End#(Length-1) end
   fun {IsEmpty _#_#Length} Length == 0 end
   fun {Preview Start#_#_} Start end
   fun {Enclose Content}
      fun {$ Message}
	 case Message
	 of enqueue(Item) then {Enclose {Enqueue Content Item}}
	 [] dequeue(Item) then {Enclose {Dequeue Content Item}}
	 [] isEmpty then {IsEmpty Content}
	 [] preview then {Preview Content}
	 else raise unsupportedOperation(Message) end end end end in
   fun {$} End in
      {Enclose !!End#End#0} end end