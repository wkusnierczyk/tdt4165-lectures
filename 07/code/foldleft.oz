fun {FoldLeft List Null Transform Combine}
   {Iterate
    state(list:List result:Null)
    fun {$ state(list:List ...)}
       List == nil end
    fun {$ state(list:Head|Tail result:Result)}
       state(list:Tail
	     result:{Combine Result {Transform Head}}) end}.result end