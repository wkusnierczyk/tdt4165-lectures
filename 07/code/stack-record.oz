fun {New}
   stack end
fun {Push Stack Item}
   stack(top:Item rest:Stack) end
fun {Pop Stack ?Item}
   Item = Stack.top
   Stack.rest end
fun {IsEmpty Stack}
   Stack == stack end