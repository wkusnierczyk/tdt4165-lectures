fun {Map List Function}
   case List of Head|Tail
   then {Function Head}|{Map Tail Function}
   else nil end end