declare

\insert shift.oz
\insert shift-cryptex.oz

Plaintexts = ["hellodolly" "funnybunny"]
Keys = [1 2 3]

Cryptexes =
{Map Keys
 fun {$ Key} {MakeCryptex Key} end}

Test =
{Map Plaintexts
 fun {$ Plaintext}
    {Map Cryptexes
     fun {$ Cryptex}
	Cryptogram = {Cryptex.encrypt Plaintext} in
	test(original:Plaintext
	     encrypted:Cryptogram
	     decrypted:{Cryptex.decrypt Cryptogram}) end} end}

{Browse Test}

