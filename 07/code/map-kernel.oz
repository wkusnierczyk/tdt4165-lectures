Map = 
proc {$ List Procedure ?Result}
   case List of Head|Tail then
      local PartialResult NewHead in
	 Result = NewHead|PartialResult
	 {Procedure Head NewHead}
	 {Map Tail Procedure PartialResult} end
   else Result = nil end end