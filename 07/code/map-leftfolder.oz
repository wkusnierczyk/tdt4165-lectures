fun {Map List Function}
   End Result#nil =
   {FoldLeft List End#End Function
    fun {$ Start#End Item}
       NewEnd in
       End = Item|NewEnd
       Start#NewEnd end} in
   Result end