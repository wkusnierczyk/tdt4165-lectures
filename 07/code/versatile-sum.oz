fun {VersatileSum Objects}
   case Objects of (_|_)|_ then
      {Mux Objects VersatileSum}
   else {Sum Objects} end end