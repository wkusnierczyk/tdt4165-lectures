New =
local
   fun {Enclose Start#End#Length}
      fun {Enqueue Item} NewEnd in
	 End = Item|!!NewEnd
	 {Enclose Start#NewEnd#(Length+1)} end
      fun {Dequeue ?Item}
	 case Start of Head|Tail then
	    Item = Head
	    {Enclose Tail#End#(Length-1)} end end
      fun {IsEmpty} Length == 0 end
      fun {Preview} Start end in
      queue(enqueue:Enqueue dequeue:Dequeue
	    isEmpty:IsEmpty preview:Preview) end
   End in
   fun {$}
      {Enclose !!End#End#0} end end