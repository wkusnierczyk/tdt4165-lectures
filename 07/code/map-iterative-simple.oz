fun {Map List Function}
   End Result#nil =
   {Iterate List#(End#End)
    fun {$ List#_}
       List == nil end
    fun {$ (Head|Tail)#(Start#End)}
       NewEnd in
       End = {Function Head}|NewEnd
       Tail#(Start#NewEnd) end}.2 in
   Result end