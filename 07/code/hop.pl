#!/usr/bin/perl

print
join ",", 
   map { $$_[1] x $$_[0] }
      sort { $$a[0] <=> $$b[0] }
         map { chomp; [split /\s/, $_, 2] } 
            grep { /^\d+\s+/ }
               <>;
