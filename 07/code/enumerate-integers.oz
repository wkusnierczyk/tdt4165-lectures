fun {EnumerateIntegers From To}
   {Enumerate From
    fun {$ Integer} Integer+1 end
    fun {$ Integer} Integer>To end} end