fun {Zip List1 List2}
   {Mux [List1 List2]
    fun {$ [Item1 Item2]} Item1#Item2 end} end
fun {Unzip List}
   {Demux List
    fun {$ Item1#Item2} [Item1 Item2] end} end