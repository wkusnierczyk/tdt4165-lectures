declare

\insert index.oz
\insert enumerate.oz
\insert shift-generic.oz

PlainAlphabet = {Enumerate &a &z}
CryptoAlphabet = {Enumerate &A &Z}

Plaintexts = ["hellodolly" "funnybunny"]
Keys = [1 2 3]

{Browse {Encrypt Plaintexts.1 1 PlainAlphabet CryptoAlphabet}}

Test =
{Map Plaintexts
 fun {$ Plaintext}
    {Map Keys
     fun {$ Key}
	Cryptogram = {Encrypt Plaintext Key PlainAlphabet CryptoAlphabet} in
	test(original:Plaintext
	     encrypted:Cryptogram
	     decrypted:{Decrypt Cryptogram Key PlainAlphabet CryptoAlphabet}) end} end}

{Browse Test}