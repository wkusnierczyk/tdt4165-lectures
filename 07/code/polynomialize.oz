fun {Polynomialize Coefficients Variables}
   {Flatten
    {Map Variables
     fun {$ Variable}
	{Map {Map Coefficients
	      fun {$ coeff(A B C)}
		 fun {$ X} A*X*X + B*X + C end end}
	 fun {$ Polynomial}
	    {Polynomial Variable} end} end}} end