fun {FoldRight List Null Transform Combine}
   case List of nil then Null
   [] Head|Tail then
      {Combine {Transform Head}
       {FoldRight Tail Null Transform Combine}} end end