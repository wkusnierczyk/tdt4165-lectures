Min = {Selector Value.min}
Max = {Selector Value.max}
Sum = {LeftFolder 0.0 Identity Number.'+'}
fun {Average Data}
   {Sum Data} / {Int.toFloat {Length Data}} end