fun {Select Head|Tail Criterion}
   {FoldRight Tail Head Identity
    fun {$ Item1 Item2}
       if {Criterion Item1 Item2} then Item1 else Item2 end end} end