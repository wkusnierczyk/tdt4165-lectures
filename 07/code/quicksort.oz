fun {Quicksort List Before}
   case List
   of Pivot|Rest then
      Below#Above = {Split Rest Pivot Before} in
      {Append {Quicksort Below Before} Pivot|{Quicksort Above Before}}
   else nil end end