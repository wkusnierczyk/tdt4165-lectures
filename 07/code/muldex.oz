fun {Mux Lists Combine}
   {Map {Transpose Lists} Combine} end
fun {Demux List Split}
   {Transpose {Map List Split}} end