declare

\insert shift.oz

Plaintexts = ["hellodolly" "funnybunny"]
Keys = [1 2 3]
Test =
{Map Plaintexts
 fun {$ Plaintext}
    {Map Keys
     fun {$ Key}
	Cryptogram = {Encrypt Plaintext Key} in
	test(original:Plaintext
	     encrypted:Cryptogram
	     decrypted:{Decrypt Cryptogram Key}) end} end}

{Browse Test}