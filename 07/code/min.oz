fun {Min List}
   case List of [Value] then Value
   [] Value|Values then
      PartialMin = {Min Values} in
      if Value<PartialMin then Value else PartialMin end end