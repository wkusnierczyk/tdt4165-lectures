fun {MakeCryptex Order}
   Key = {Zip {Enumerate &a &z}
	  {Unzip
	   {Quicksort
	    {Zip {Enumerate &A &Z} Order}
	    fun {$ _#N _#M} N<M end}}.1}
   ReverseKey = {Map
		 {Quicksort Key
		  fun {$ _#N _#M} N<M end}
		 fun {$ Plain#Crypto} Crypto#Plain end}
in
   cryptex(
      encrypt:fun {$ Plaintext} {Encrypt Plaintext Key} end
      decrypt:fun {$ Cryptogram} {Decrypt Cryptogram ReverseKey} end)
end