fun {Enumerate Start Next Stop}
   if {Stop Start} then nil
   else Start|{Enumerate {Next Start} Next Stop} end end