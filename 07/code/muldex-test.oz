declare
\insert sources.oz

V1 = [1. 2. 3.]
V2 = [4. 5. 6.]
M1 = [V1 V2]
M2 = [V2 V1]

declare

fun {Product List}
   {FoldLeft List 1.0 Identity Number.'*'} end

fun {VersatileProduct Objects}
   {VersatileCombine Objects Product} end
    
M = [[1.0 2.0] [3.0 4.0]]

{Browse {MatrixProduct M M}}