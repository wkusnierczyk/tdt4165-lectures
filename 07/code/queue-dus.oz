local Safe = {Wrapper} in
   fun {New} End in
      {Safe.wrap End#End#0} end
   fun {Enqueue Queue Item}
      case {Safe.unwrap Queue} of Start#End#Length then
	 NewEnd in
	 End = Item|NewEnd
	 {Safe.wrap Start#NewEnd#(Length+1)} end end
   fun {Dequeue Queue ?Item}
      case {Safe.unwrap Queue} of (Head|Tail)#End#Length then
	 Item = Head
	 {Safe.wrap Tail#End#(Length-1)} end end
   fun {IsEmpty Queue}
      {Safe.unwrap Queue}.3 == 0 end end