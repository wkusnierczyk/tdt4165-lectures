fun {Enumerate Start Next Stop Transform}
   if {Stop Start} then nil
   else
      {Transform Start}
      |{Enumerate {Next Start} Next Stop Transform} end end