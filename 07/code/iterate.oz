fun {Iterate State IsFinal Update}
   if {IsFinal State} then State
   else {Iterate {Update State} IsFinal Update} end end