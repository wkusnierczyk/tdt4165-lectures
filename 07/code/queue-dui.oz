fun {New}
   End in
   End#End#0 end
fun {Enqueue Start#End#Length Item}
   NewEnd in
   End = Item|NewEnd
   Start#NewEnd#(Length+1) end
fun {Dequeue (Head|Tail)#End#Length ?Item}
   Item = Head
   Tail#End#(Length-1) end
fun {IsEmpty _#_#Length}
   Length == 0 end