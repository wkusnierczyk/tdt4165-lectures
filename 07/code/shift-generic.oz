fun {Encrypt Plaintext Key PlainAlphabet CryptoAlphabet}
   AlphabetSize = {Length PlainAlphabet} in
   {Map Plaintext
    fun {$ Character}
       {Nth CryptoAlphabet
	({Index PlainAlphabet Character} + Key - 1)
	mod AlphabetSize + 1} end} end
fun {Decrypt Cryptogram Key PlainAlphabet CryptoAlphabet}
   AlphabetSize = {Length PlainAlphabet} in
   {Map Cryptogram
    fun {$ Character}
       {Nth PlainAlphabet
	({Index CryptoAlphabet Character} - Key + AlphabetSize - 1)
	mod AlphabetSize + 1} end} end