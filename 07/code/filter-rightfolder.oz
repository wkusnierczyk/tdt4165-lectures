fun {Filter List Criterion}
   {FoldRight List nil Identity
    fun {$ Item Rest}
       if {Criterion Item} then Item|Rest
       else Rest end end} end
