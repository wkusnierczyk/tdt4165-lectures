(define (zip list1 list2)
   (if (null? list1) '()
       (cons (cons (car list1) (car list2))
             (zip (cdr list1) (cdr list2)))))
(define (unzip list)
   (if (null? list) (values '() '())
       (call-with-values 
          (lambda () (unzip (cdr list)))
          (lambda (tail1 tail2) 
             (values (cons (caar list) tail1) 
                     (cons (cdar list) tail2))))))
