fun {Wrapper}
   SecretKey = {NewName}
   fun {Wrap Object}
      fun {$ Key}
	 if Key == SecretKey then Object end end end
   fun {Unwrap SecureObject}
      {SecureObject SecretKey} end in
   wrapper(wrap:Wrap
	   unwrap:Unwrap) end