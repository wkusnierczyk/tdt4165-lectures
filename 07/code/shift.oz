fun {Encrypt Plaintext Key}
   {Map Plaintext
    fun {$ Character}
       (Character - &a + Key) mod 26 + &A end} end
fun {Decrypt Cryptogram Key}
   {Map Cryptogram
    fun {$ Character}
       (Character - &A - Key + 26) mod 26 + &a end} end