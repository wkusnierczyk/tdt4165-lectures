(define-syntax show
	(syntax-rules ()
		((_ expression) 
			(begin
				(display 'expression)
				(display ": ")
				(display expression)
				(newline)))))

(define value 0)
(define double
	(lambda (value)
		(* value value)))
			
(show value)
(show (double value))
(show value)
