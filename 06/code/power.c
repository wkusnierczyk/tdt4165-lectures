#include <stdio.h>

int power(int base, int exponent) {
	int partial;
	if (0 == exponent) return 1;
	partial = power(base, exponent - 1);
	return base * partial;
}

int main() {
	int base = 3;
	int exponent = 4;
	printf("%i^%i = %i\n", base, exponent, power(base, exponent));
	return 0;
}
