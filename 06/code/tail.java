class tail {

	private static int tail(int a, int b) {
		if (a < 1000) return tail(a+1, b);
		if (b < 1000) return tail(0, b+1);
		else return 0;
	}
	
	public static void main(String[] args) {
		tail(0,0);
	}
	
}
