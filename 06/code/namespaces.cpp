#include <iostream>

namespace integers {
	int div(int x, int y) {
		return x / y;
	}
}

namespace floats {
	double div(double x, double y) {
		return x / y;
	}
}

int main() {
	using namespace std;
	cout << integers::div(1,2) << ", " << floats::div(1,2) << endl;
	return 0;
}
