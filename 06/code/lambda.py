#!/usr/bin/python

value = 0
double = lambda value: 2*value

print "value: " + str(value)
print "double(1): " + str(double(1))
print "value: " + str(value)
