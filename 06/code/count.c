#include <stdio.h>

// global (thus implicitly static) variable
int x = 10;

int count(int value) {
	// local, explicitly static variable, initialized only once at the load time
	static count = 0;
	count += value;
	return count;
}

int main() {
	printf("%i\n",count(x));
	printf("%i\n",count(x));
	printf("%i\n",count(x));
	return 0;
}
