val value = 0
val double = (value: int) => 2*value

println("value: " + value)
println("double(1): " + double(1))
println("value: " + value)
