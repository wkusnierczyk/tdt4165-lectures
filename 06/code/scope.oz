declare

% global variable
Variable = 'global'

% procedure for displaying the global Variable
proc {BrowseVariable}
   {Browse Variable}
end

% introducing local Variable
local Variable='local' in
   {Browse Variable}
   % display the global Variable (lexical scoping!)
   {BrowseVariable}
end

declare

% remap the identifier
Variable = redeclared

% display the global Variable -- previous value (the closure keeps old mapping)
{BrowseVariable}

