(define (make-adder n)
	(lambda (m) 
		(+ m n)))
		
(define 3+ (make-adder 3))
(display (3+ 1))

(environment-assign! 
	(procedure-environment 3+)
	'n
	0)
(display (3+ 1))
