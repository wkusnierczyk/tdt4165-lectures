;; a global variable
(define variable 'global)

;; a procedure for displaying the global
(define (display-variable) 
	(display variable) (newline))

;; display the variable
(display-variable)

;; modify the variable
(define variable 'modified)

;; display the variable again
(display-variable)
