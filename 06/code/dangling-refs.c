#include <stdio.h>
#include <stdlib.h>

typedef struct { int x, y; } Point;

Point * f1(int x, int y) {
	Point point = { x, y };
	// ops, point will be automatically deallocated
	return &point;
}

Point * f2(int x, int y) {
	Point * point = (Point *) malloc(sizeof(Point));
	point->x = x; point->y = y;
	return point;
}

int main() {
	Point * p1 = f1(1,2); // dangling reference
	Point * p2 = f1(3,4); // dangling reference
	Point * p3 = f2(5,6);
	printf("p%i = (%i, %i)\n", 1, p1->x, p1->y);
	printf("p%i = (%i, %i)\n", 2, p2->x, p2->y);
	printf("p%i = (%i, %i)\n", 3, p3->x, p3->y);
	free(p3);
	p3 = NULL;
	return 0;
}
