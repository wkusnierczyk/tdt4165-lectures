(define (product list)
	(call-with-current-continuation
		(lambda (continuation)
			(let product ((list list))
				(cond ((null? list) 1)
					((zero? (car list)) (continuation 0))
					(else (* (car list) (product (cdr list)))))))))

(display (product '(1 2 3)))

'--done
