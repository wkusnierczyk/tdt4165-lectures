#!/usr/bin/perl

# a global variable
$variable = "global";

# a procedure for printing the content of the global variable
sub show {
	print "$variable\n";
}

# a procedure using the global $variable
sub global {
	our $variable = "our";
	&show;
}

# a procedure using lexically scoped local $variable
sub lexical {
	my $variable = "my";
	&show;
}

# a procedure using dynamically scoped local $variable
sub dynamic {
	local $variable = "local";
	&show;
}

# simple tests
print "testing global...\n";
&show();
&global();
&show();

print "testing lexical...\n";
&show();
&lexical();
&show();

print "testing dynamic...\n";
&show();
&dynamic();
&show();