#include <stdio.h>

int product(const int * numbers, int count) {
	if (0 == count)
		return 1;
	if (0 == *numbers)
		return 0;
	return *numbers * product(numbers + 1, count - 1);
}

int main() {
	int numbers[] = {1,2,3};
	printf("product: %i\n", product(numbers, 3));
	return 0;
}
