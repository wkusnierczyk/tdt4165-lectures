functor
import
   System(show:Browse)
   Application(exit:Exit)
define
   fun {Recursive A B C}
      if A < 1000 then
	 0 + {Recursive A+1 B C}
      elseif B < 1000 then
	 0 + {Recursive 0 B+1 C}
      elseif C < 1000 then
	 {Browse C}
	 0 + {Recursive 0 0 C+1}
      else 0
      end
   end
   _ = {Recursive 0 0 0}
   {Exit 0}
end