import java.util.*;

class Point {
	
	private int x, y;
	private static Map<Integer, Point> points = new HashMap<Integer, Point>();
	
	private Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
	
	public static Point getInstance(int x, int y) {
		int key = 2*x + 3*y;
		if (points.containsKey(key)) return points.get(key);
		Point point = new Point(x, y);
		points.put(key, point);
		return point;
	}
	
	public static void main(String[] args) {
		Point p1, p2;
		p1 = Point.getInstance(1,2);
		p2 = Point.getInstance(1,2);
		System.out.println("p1 = " + p1);
		System.out.println("p2 = " + p2);
		System.out.println("p1 " + (p1 == p2 ? "==" : "!=" ) + " p2");
	}
	
}
