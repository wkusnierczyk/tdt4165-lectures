# computes (or not) the cube of the argument
cube <- function(n) {
	square <- function() n*n
	n*square()
}

# what is the cube of 2?
cube(2)
