functor
import 
	System(show:Browse)
	Application(exit:Exit)
define
	Value = 0
	Double = fun {$ Value} 2*Value end
	{Browse 'value'#Value}
	{Browse '{Double 1}'#{Double 1}}
	{Browse 'value'#Value}
	{Exit 0}
end
