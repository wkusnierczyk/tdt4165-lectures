functor
import
   System(show:Browse)
   Application(exit:Exit)
define
   fun {Iterative A B C Acc}
      if A < 1000 then
	 {Iterative A+1 B C 0+Acc}
      elseif B < 1000 then
	 {Iterative 0 B+1 C 0+Acc}
      elseif C < 1000 then
	 {Browse C}
	 {Iterative 0 0 C+1 0+Acc}
      else Acc
      end
   end
   _ = {Iterative 0 0 0 0}
   {Exit 0}
end