;; a global variable
(define variable 'global)

;; procedure for displaying the global variable
(define (display-variable) (display variable) (newline))

;; lexical scoping
(display "\nlexical scoping\n")
(display-variable)
(let ((variable 'local))
  (display variable) (newline)
  (display-variable))
(display-variable)

;; dynamic scoping
(display "\ndynamic scoping\n")
(display-variable)
(fluid-let ((variable 'local))
  (display variable) (newline)
  (display-variable))
(display-variable)

(define (foo n) n)
(let ((foo (lambda (n) 
