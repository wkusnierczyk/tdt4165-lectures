#include <stdio.h>
#include <stdlib.h>

typedef struct { int x, y; } Point;

Point * point_new(int x, int y) {
	Point * point = (Point *) malloc(sizeof(Point));
	point->x = x;
	point->y = y;
	return point;
}

int main() {
	Point * point = point_new(1,2);
	printf("point = (%i, %i)\n", point->x, point->y);
	free(point);
	point = NULL;
	return 0;
}	
