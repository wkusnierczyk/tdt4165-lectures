#include <stdio.h>

typedef struct { int x, y; } Point;

Point point_stack(int x, int y) {
	Point point;
	point.x = x;
	point.y = y;
	return point;
}

Point * point_heap(int x, int y) {
	Point * point = new Point;
	point->x = x;
	point->y = y;
	return point;
}

int main() {
	Point point;
	point = point_stack(1,2);
	printf("point = (%i, %i)\n", point.x, point.y);
	Point * ppoint;
	ppoint = point_heap(1,2);
	printf("point = (%i, %i)\n", ppoint->x, ppoint->y);
	delete ppoint;
	return 0;
}
