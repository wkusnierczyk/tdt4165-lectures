#!/usr/bin/ruby

value = 0
double = lambda { |value| 2*value }

puts "value: " + value.to_s
puts "double(1): " + double.call(1).to_s
puts "value: " + value.to_s

