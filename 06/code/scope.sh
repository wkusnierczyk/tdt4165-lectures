#!/bin/bash

shopt -s -o nounset

# a global variable
variable=global

# a function for displaying $variable
function show() { 
	echo $variable
}

# a function for testing -- which $variable is displayed?
function test() {
	local variable=local
	show
}

# a simple test
show
test
show

# all fine
return 0
