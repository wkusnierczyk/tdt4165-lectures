functor
import
   System
   Application
define
   fun {MDCRecognize Input}
      fun {Terminate State}
	 {Member State [cmd op int]}
      end
      fun {Iterate State Input}
	 case Input
	 of 10|nil then {Terminate State}
	 [] nil then {Terminate State}
	 [] Head|Tail then
	    if State == start then
	       if {Member Head "pf"} then
		  {Iterate cmd Tail}
	       elseif {Member Head "+-*/"} then
		  {Iterate op Tail}
	       elseif {Member Head "0123456789"} then
		  {Iterate int Tail}
	       else false
	       end
	    elseif State == cmd orelse State == op then
	       false
	    elseif State == int then
	       if {Member Head "0123456789"} then
		  {Iterate int Tail}
	       else false
	       end
	    end
	 end
      end
   in
      {Iterate start Input}
   end
   Args
   try
      Args = {Application.getArgs list(mode:anywhere)}
   catch Exception then
      {System.show "mdc-recognizer: "#Exception.1.2}
      {Application.exit 0}
   end
   {ForAll Args
    proc {$ Arg}
	   {System.show {MDCRecognize Arg}}
	end}
   {Application.exit 0}
end
