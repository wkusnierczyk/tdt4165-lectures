functor
import
   System
   Application
define
   fun {MDCScan Input}
      fun {Iterate State Seen Input Output}
	 fun {Terminate}
	    case State
	    of start then Output
	    [] int then int(Seen)|Output
	    end
	 end
      in
	 case Input
	 of nil then {Terminate}
	 [] 10|nil then {Terminate}
	 [] Char|Rest then
	    case State
	    of start then
	       if {List.member Char " "} then
		  {Iterate start nil Rest Output}
	       elseif {List.member Char "pf"} then
		  {Iterate start nil Rest cmd(Char)|Output}
	       elseif {List.member Char "+-*/"} then
		  {Iterate start nil Rest op(Char)|Output}
	       elseif {List.member Char "0123456789"} then
		  {Iterate int Char|Seen Rest Output}
	       else
		  error(Char)|Output
	       end
	    [] int then
	       if {List.member Char "0123456789"} then
		  {Iterate int Char|Seen Rest Output}
	       else
		  {Iterate start nil Input int(Seen)|Output}
	       end
	    end
	 end
      end
      fun {Convert Token}
	 case Token
	 of cmd(Cmd) then cmd({Char.toAtom Cmd})
	 [] op(Op) then op({Char.toAtom Op})
	 [] int(Int) then int({String.toInt {List.reverse Int}})
	 [] error(Error) then error({Char.toAtom Error})
	 else Token
	 end
      end
   in
      {List.reverse
       {List.map
	{Iterate start nil Input nil}
	Convert}}
   end
   Args
   try
      Args = {Application.getArgs list(mode:anywhere)}
   catch Exception then
      {System.show "mdc-scanner: "#Exception.1.2}
      {Application.exit 0}
   end
   {ForAll Args
    proc {$ Arg}
       {System.show {MDCScan Arg}}
    end}
   {Application.exit 0}
end
