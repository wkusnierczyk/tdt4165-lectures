\switch +gump

functor

import
   System
   Application
   GumpScanner

define

	\insert MDCScanner.ozg

   fun {Scan Input}
      Scanner = {New MDCScanner init()}
      fun {GetTokens} Class Lexeme in 
	 {Scanner getToken(?Class ?Lexeme)}
	 case Class of 'EOF' then nil
	 else Class(Lexeme)|{GetTokens}
	 end 
      end 
      {Scanner scanVirtualString(Input)}
      Tokens = {GetTokens}
      {Scanner close()}
   in
      Tokens
   end
   
   Args
   try
      Args = {Application.getArgs list()}
   catch Exception then
      {System.show "mdc-gump: "#Exception.1.2}
      {Application.exit 0}
   end
   {ForAll Args
    proc {$ Arg}
       {System.show {Scan Arg}}
    end}
   {Application.exit 0}

end
