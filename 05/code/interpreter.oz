class OzInterpreter

   attr Debug

   meth init()
      Debug := false
   end

   meth setDebug()
      Debug := true
   end
   meth unsetDebug()
      Debug := false
   end

   meth debug(Message)
      if @Debug then
         {System.printInfo 'I | '}
         {System.show Message}
      end
   end

   meth process(Program)
      proc {Process Stack Store}
         case Stack of
            nil then skip
         [] (S#E)|Stack then
            {self debug(pop(stat:S env:E))}
            case S of
               noop then
               {self debug(pass)}
               {Process Stack Store}
            [] seq(S1 S2) then
               {self debug(push(stat:S2 env:E))}
               {self debug(push(stat:S1 env:E))}
               {Process (S1#E)|(S2#E)|Stack Store}
            [] loc(id:I stat:S) then
               V = {List.length Store} + 1
               NE = (I#V)|E in
               {self debug(map(id:I var:V))}
               {self debug(push(stat:S env:NE))}
               {Process (S#NE)|Stack {List.append Store [_]}}
            end
         else
            {System.showInfo 'error'}
         end
      end
   in
      {Process [Program#nil] nil}
   end

end